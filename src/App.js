import React, { Component } from 'react';
import 'reset-css/reset.css'
import './App.css';

const textColor = '#fff';
const defaultStyle = {
  color: textColor,
}
const counterStyle = {
  ...defaultStyle,
  fontSize: '20px',
  lineHeight: '40px',
  display: 'inline-block',
  width: '40%',
}

const fakeServerData = {
  user: {
    name: 'William',
    playlists: [
      {
        name: 'My favs',
        songs: [{name: 'snergen', duration: 1345}, {name: 'flergen', duration: 1345}],
      }
    ]
  }
}

class PlaylistCounter extends Component {
  render() {
    return(
      <div style={{...counterStyle}}>
        <h2>{this.props.playlists.length} Playlists foos</h2>
      </div>
    );
  }
}

class HoursCounter extends Component {
  render() {
    const allSongs = this.props.playlists.reduce((songs, eachplaylist) => {
      return songs.concat(eachplaylist.songs)
    }, []);
    console.log('allsongs: ', allSongs);
    const totalDuration = allSongs.reduce((sum, eachSong) => {
      return sum + eachSong.duration
    }, 0);
    const durationHours = Math.floor(totalDuration/60);
    const hoursStyle = {
      ...counterStyle,
      color: durationHours < 10 ? 'red' : 'green'
    }
    return(
      <div style={hoursStyle}>
        <h2>{durationHours} hours</h2>
      </div>
    );
  }
}

class Filter extends Component {
  render() {
    return(
      <div>
        <input onKeyUp={e => this.props.onTextChange(e.target.value)} type="text"/>
      </div>
    );
  }
}

class Playlist extends Component {
  render() {
    const { name, songs, image } = this.props.playlist;
    return (
      <div style={{ ...defaultStyle, width: '25%', display: 'inline-block', }}>
        <img alt="temp" src={image} />
        <h3>{name}</h3>
        <ul>
          {songs.map(s => (
            <li>{s.name}</li>
          ))}
        </ul>
      </div>
    )
  }
}

class App extends Component {
  state = {
    serverData: {},
    filterString: '',
  }

  componentDidMount() {
    const token = new URLSearchParams(document.location.search).get('access_token');
    if (!token) return;
    fetch('https://api.spotify.com/v1/me', {
      headers: { 'Authorization': 'Bearer ' + token }
    })
    .then(res => res.json())
    .then(res => {console.log('res from /me:', res); return res})
    .then(res => this.setState({user: {name: res.display_name}}));

    fetch('https://api.spotify.com/v1/me/playlists', {
      headers: { 'Authorization': 'Bearer ' + token }
    })
    .then(res => res.json())
    .then(playlistData => {
      let playlists = playlistData.items;
      let trackDataPromises = playlists.map(playlist => {
        let responsePromise =  fetch(playlist.tracks.href, {
          headers: { 'Authorization': 'Bearer ' + token }
        })
        let trackDataPromise = responsePromise.then(res => res.json())
        return trackDataPromise;
      })
      let allTracksDatasPromises = 
        Promise.all(trackDataPromises)
      let playlistsPromise = allTracksDatasPromises.then(trackDatas => {
        trackDatas.forEach((trackData, idx) => {
          playlists[idx].trackDatas = trackData.items
            .map(item => item.track)
            .map(item => ({
              name: item.name,
              duration: item.duration_ms /1000
            }))
        })
        return playlists;
      })
      return playlistsPromise;
    })
    .then(playlists => this.setState({
        playlists: playlists.map(item => {
          console.log('item.trackDatas', item.trackDatas)
          return {
            name: item.name,
            image: item.images.find(i => i.width == 300).url,
            songs: item.trackDatas.slice(0,10),
          }
        })
      }
    ));
  }
  
  render() {

    const playlistsToRender =
      this.state.user &&
      this.state.playlists
        ? this.state.playlists.filter(pl => {
          console.log('pl:', pl)
          const songsStr = pl.songs.find(song => song.name.toLowerCase().includes(this.state.filterString.toLowerCase()));
          console.log(songsStr)
          // if pl.name or pl.songs.name
         return pl.name.toLowerCase().includes(
            this.state.filterString.toLowerCase()
          ) || songsStr
        })
        : [];
    
    return (
      <div className="App">
        {this.state.user ? 
          <div>
            <h1 style={{'font-size': '45px'}}>Hello {this.state.user.name}</h1>
            <PlaylistCounter playlists={playlistsToRender} />
            <HoursCounter playlists={playlistsToRender} />
            <Filter onTextChange={text => this.setState({ filterString: text})}/>
            {playlistsToRender.map(pl => <Playlist playlist={pl}/>)}  
          </div> : <button onClick={() => {
            window.location = window.location.href.includes('localhost') ? 'http://localhost:8888/login' : 'https://better-playlists-bk-backend.herokuapp.com/login'
          }}>Sign in to Spotify</button>
        }
      </div>
    );
  }
}

export default App;
